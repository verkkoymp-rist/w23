import Reporter from './Reporter';

const App = () => {
  return (
    <div>
      <Reporter name="Antero Mertaranta">
        Löikö mörkö sisään?
      </Reporter>
      <Reporter name="Kevin McGran">
        I know it's a rough time now, but did you at least enjoy playing in the tournament
      </Reporter>
    </div>
  );
};

export default App;
