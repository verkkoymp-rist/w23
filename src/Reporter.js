const Reporter = ({ name, children }) => {
    return (
      <div>
        <strong>{name}</strong>
        <p>{children}</p>
      </div>
    );
  };
  
  export default Reporter;
  